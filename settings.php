<?php
session_start();
include('includes/functions.php');
$userID = $_SESSION['loggedInUserID'];
if  ( !$_SESSION['loggedInUser'] ) {
    header("Location: index.php");
}
if ( isset( $_POST['save'] ) ) { #if user pressed the save button

    if ( !$_POST['newname'] ) { #if the newname input isn't set
        $nameError = "<strong>nickname </strong>"; #generating nickname error
    } else {
        $signUpName = validateFormData( $_POST['newname'] );
    }

    if ( !$_POST['newemail'] ) {
        $emailError = "<strong>email </strong>"; #generating email error
    } else {
        $signUpEmail = validateFormData( $_POST['newemail'] );
        #extra if statement that prevents our custom function from finding our existing email and returning a false error
        if ( strcmp( $signUpEmail, $_SESSION['loggedInUserEmail'] ) != 0 ) { #using strcmp() to compare vars (case insensitive)
            $signUpEmail = checkDuplicateEmail( $existingEmail, $signUpEmail ); #custom function that checks for duplicate email entry into database
        }
    }

    if ( ($_POST['oldpass'] == '' && $_POST['newpass']) || ( $_POST['newpass'] == '' && $_POST['oldpass'] ) ) { #if the user only fills out one password input (if both are left empty then the old pass is kept)
         $oldPassError = "<strong>Old/new password field missing</strong>.";  #error generated
    } elseif ( $_POST['oldpass'] && $_POST['newpass'] ) { #if both pass inputs had values
        if ( password_verify( $_POST['oldpass'], $_SESSION['loggedInUserPass'] ) ) { #verifying user's old pass with their SESSION pass
            #password was successfully verified
            $signUpPass = validateFormData( $_POST['newpass'] ); #new pass is validated and ready for storage
        } else { #old pass didn't match session pass
           $passMismatchError = "Old password <strong>does not match</strong> our records.";
        }
    }

    #checking if name and email values are validated and if custom function checkDuplicateEmail() didn't return "error"
    if ( $signUpName && $signUpEmail && $signUpEmail != "error" && !$oldPassError && !$passMismatchError ) {
        # Success!
        if ( $signUpPass ) { # checking if a new password is ready for storage
            $updatedHashPass = password_hash( $signUpPass, PASSWORD_DEFAULT ); #hashing the new password for storage
            include('includes/connection.php');
            #query for storing the new password
            $query = "UPDATE users
                    SET password ='$updatedHashPass'
                        WHERE id='$userID'";
            $resultStored = mysqli_query( $conn, $query );

            $_SESSION['loggedInUserPass'] = $updatedHashPass; #the new password is now stored in the SESSION var
         }

        include('includes/connection.php');
        $query = "UPDATE users
            SET email ='$signUpEmail',
            name ='$signUpName'
            WHERE id='$userID'"; #user data is updated in db here
        $resultsStored = mysqli_query( $conn, $query ); #storing the result
        $_SESSION['loggedInUser']       = $signUpName; #Assigning $_SESSION vars to new user data
        $_SESSION['loggedInUserEmail']  = $signUpEmail;
        #Redirects user to clients page with 'updateduser' string query that alerts user they updated their data
        header("Location: clients.php?alert=updateduser");
    } elseif( $signUpEmail == "error") {#duplicate email address was found in the 'users' table
        $duplicateError = "<div class='alert alert-danger'>Error!! Email address/account already exists! Please choose a different email address</div>";

    } elseif( $oldPassError || $passMismatchError ) { #if a password error was created earlier it's passed here to $passError.
        $passError = "<div class='alert alert-danger'>Error! " . $oldPassError . $passMismatchError . " Try again.<a class='close' data-dismiss='alert'>×</a></div>";
    } else {#error vars are created above in Null checks and are collected here to be displayed in a single error message.
        $signUpError = "<div class='alert alert-danger'>Missing field(s)! Please fill out: " . $nameError . $emailError . "<a class='close' data-dismiss='alert'>×</a></div>";
    }
}

if(isset($conn)){
    mysqli_close($conn);
}
include('includes/header.php');
?>
<?php
#all error messages are displayed here
if ( isset( $duplicateError ) ) {
    echo $duplicateError;
} else {
    $duplicateError = '';
}
if ( isset( $signUpError ) ) {
    echo $signUpError;
} else {
    $signUpError = '';
}
if ( $passError ) {
    echo $passError;
} else {
    $passError = '';
}
?>

<h1>Settings</h1>
<p class="lead">Update your personal information</p>
<div class="row">
   <div class="col-sm-6 col-xs-12">
    <form action="<?php echo htmlspecialchars( $_SERVER["PHP_SELF"] ); ?>" method="post">
        <div class="form-group">
            <label for="signup-name-new">Nickname<span style="color:red;"> *</span></label>
            <input type="text" class="form-control" id="signup-name-new" placeholder="nickname" name="newname" value="<?php echo $_SESSION['loggedInUser']; ?>">
        </div>
        <div class="form-group">
            <label for="signup-email-new">Email<span style="color:red;"> *</span></label>
            <input type="email" class="form-control" id="signup-email-new" placeholder="email" name="newemail" value="<?php echo $_SESSION['loggedInUserEmail']; ?>">
        </div>
        <div class="form-group">
            <label for="signup-pass-new">Update password</label><br>
            <input type="password" class="form-control" id="signup-pass-new" placeholder="enter old password" autocomplete="off" name="oldpass" minlength="8" style="width: 49.5%; display: inline;">
            <input type="password" class="form-control pull-right" id="signup-pass-confirm" placeholder="create new password" name="newpass" autocomplete="new-password" minlength="8" style="width: 49.5%; display: inline;">
        </div>
        <button type="submit" class="btn btn-primary" name="save">Save</button>
        <a href="clients.php" type="button" class="btn btn-danger" name="cancel">Cancel</a>
    </form>
      <br>
       * <small>Required fields</small>
 </div>
</div>

<?php // Don't forget to include the footer
include('includes/footer.php');
?>