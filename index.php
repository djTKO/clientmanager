<?php
session_start();

include('includes/functions.php');

// if login form was submitted
if( isset( $_POST['login'] ) ) {

    // create variables
    // wrap data with validate function
    $formEmail = validateFormData( $_POST['email'] );
    $formPass = validateFormData( $_POST['password'] );

    // connect to database
    include('includes/connection.php');

    // create query
    $query = "SELECT name, password FROM users WHERE email='$formEmail'";

    // store the result
    $result = mysqli_query( $conn, $query );

    // verify if result is returned
    if( mysqli_num_rows($result) > 0 ) {

        // store basic user data in variables
        while( $row = mysqli_fetch_assoc($result) ) {
            $userid     = $row['id'];
            $name       = $row['name'];
            $email      = $row['email'];
            $hashedPass = $row['password'];

        }

        // verify hashed password with submitted password
        if( password_verify( $formPass, $hashedPass ) ) {

            // correct login details!
            // store data in SESSION variables
            $_SESSION['loggedInUser'] = $name;
            $_SESSION['loggedInUserID'] = $userid;
            $_SESSION['loggedInUserEmail']  = $email; // storing user email in session
            $_SESSION['loggedInUserPass']   = $hashedPass; // storing user hashedpass in session


            // redirect user to clients page
            header( "Location: clients.php" );
        } else { // hashed password didn't verify

            // error message
            $loginError = "<div class='alert alert-danger'>Wrong username / password combination. Try again.</div>";
        }

    } else { // there are no results in database

        // error message
        $loginError = "<div class='alert alert-danger'>No such user in database. Please try again. <a class='close' data-dismiss='alert'>&times;</a></div>";
    }

}

// BEGIN SIGN UP CODE
// Checks if the Sign Up button was submitted
if ( isset( $_POST['signup'] ) ) {

    // This is a null check to see if email input was submitted
    if ( !$_POST["signupemail"] ) {
        // generating error
        $emailError = "<strong>email </strong>";
    } else {
        $signUpEmail = validateFormData( $_POST["signupemail"] );
    }

    // This is a null check to see if name input was submitted
    if ( !$_POST["signupname"] ) {
        // generating error
        $nameError = "<strong>nickname </strong>";
    } else {
        $signUpName = validateFormData( $_POST["signupname"] );
    }

    // This is a null check to see if password input was submitted
    if ( !$_POST["signuppass"] ) {
        // generating error
        $passError = "<strong>password</strong>.";
    } else {
        $signUpPass = validateFormData( $_POST["signuppass"] );
    }

    // custom function that checks for duplicate email entry into database
    $signUpEmail = checkDuplicateEmail( $existingEmail, $signUpEmail );

    // Checking if all three inputs have values
    // Also checking to see if checkDuplicateEmail() returned the string "error"
    if ( $signUpEmail && $signUpName && $signUpPass && ($signUpEmail != "error") ) {
        // Success!
        // Hashing the user's password for storage in db
        $signUpHashPass = password_hash( $signUpPass, PASSWORD_DEFAULT );
        include('includes/connection.php');
        // Query used to insert a new entry into the 'clients' table
        $query = "INSERT INTO users (id, email, name, password) VALUES ( null, '$signUpEmail', '$signUpName', '$signUpHashPass')";
        // Storing the result in case we need it
        $result = mysqli_query( $conn, $query );
        // Setting new user's nickname to loggedInUser for new session.
        $_SESSION['loggedInUser'] = $signUpName;
        // Creating a new session var called newUser that clients.php will check for to display a new user welcome message.
        $_SESSION['newUser'] = $signUpName;
        // new query that retrieves new user values for storage in session variables
        // also confirms that new user data was stored correctly in table
        include('includes/connection.php');
        $query = "SELECT * FROM users WHERE email='$signUpEmail'";
        $result = mysqli_query( $conn, $query );
        if ( mysqli_num_rows( $result ) > 0 ) {
            while( $row = mysqli_fetch_assoc( $result ) ) {
                $userid      = $row['id'];
                $email       = $row['email'];
            }
        // Assigning session vars to new user data
        $_SESSION['loggedInUserID']     = $userid;
        $_SESSION['loggedinUserEmail']  = $email;
        // Redirecting new user to clients page with 'welcome' string for welcome message
        header("Location: clients.php?alert=welcome");

        } else { // error generated if new user's details were somehow not stored when they clicked Sign Up;
            $signUpError = "<div class='alert alert-danger'>An error occurred please try again later</div>";
        }
    } elseif ( $signUpEmail == "error") {// "error" string returned means that a duplicate email address was found in the 'users' table
        $duplicateError = "<div class='alert alert-danger'>Error!! Email address/account already exists! Please choose a different email or log into your existing account</div>";
    } else {// Error vars are created above in Null checks and are collected here to be displayed in a single error message.
        $signUpError = "<div class='alert alert-danger'>Missing fields! Please fill out: " . $emailError . $nameError . $passError . "<a class='close' data-dismiss='alert'>×</a></div>";
    }
}
// END SIGN UP CODE
if(isset($conn)){
    mysqli_close($conn);
}
include('includes/header.php');
?>
<?php
// Error message section:
// (NOTE: Should be recreated using a switch case)
if ( isset( $duplicateError ) ) {
    echo $duplicateError;
} else {
    $duplicateError = '';
}
if ( isset( $loginError ) ) {
    echo $loginError;
} else {
    $loginError = '';
}
if ( isset( $signUpError ) ) {
    echo $signUpError;
} else {
    $signUpError = '';
}
?>


<div class="row">
       <div class="col-sm-6" id="signin-col">
        <h1>Welcome</h1>
        <p class="lead">Log in to your account</p>
        <form class="form-inline" action="<?php echo htmlspecialchars( $_SERVER['PHP_SELF'] ); ?>" method="post">
            <div class="form-group">
                <label for="login-email" class="sr-only">Email</label>
                <input type="email" class="form-control" id="login-email" placeholder="email" name="email" value="<?php echo $formEmail; ?>">
            </div>
            <div class="form-group">
                <label for="login-password" class="sr-only">Password</label>
                <input type="password" class="form-control" id="login-password" placeholder="password" name="password" minlength="8">
            </div>
            <button type="submit" class="btn btn-primary" name="login">Login</button>
        </form>
        <br><br>
        </div> <!--END signin-col -->

        <div class="col-sm-6" id="signup-col">
            <h1>Sign up</h1>
            <p class="lead">Create a new account</p>
            <form id="signup-form" action="<?php echo htmlspecialchars( $_SERVER['PHP_SELF'] ); ?>" method="post" autocomplete="off">
                    <div class="form-group">
                    <label for="signup-name" class="sr-only">Nickname</label>
                    <input type="text" class="form-control" id="signup-name" placeholder="nickname" name="signupname">
                </div>
                <div class="form-group">
                    <label for="signup-email" class="sr-only">Email</label>
                    <input type="email" class="form-control" id="signup-email" placeholder="email" name="signupemail">
                </div>
                <div class="form-group">
                    <label for="signup-pass" class="sr-only">Create password</label>
                    <input type="password" class="form-control" id="signup-pass" placeholder="create password (8 characters min)" name="signuppass" autocomplete="new-password" minlength="8">
                </div>
            <div class="row">
                <div class="col-xs-6">
                    <p><small>Signing up is quick and free and your information is kept secure. By clicking Sign Up, you agree to our <a>Terms</a>, including our <a>Cookie</a> use.</small></p>
                </div>
            </div>
                <button type="submit" class="btn btn-primary" name="signup">Sign Up</button>
            </form><!-- END signup-form -->
        </div> <!--END signup-col -->
    </div> <!--END row -->
<?php
include('includes/footer.php');
?>