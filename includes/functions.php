<?php
// FUNCTIONS.php

// clean the form data to prevent injections

/*  Built in functions used:
    trim()
    stripslashes()
    htmlspecialchars()
    strip_tags()
    str_replace()
*/

function validateFormData($formData) {
    $formData = trim( stripslashes( htmlspecialchars( strip_tags( str_replace( array( '(', ')' ), '', $formData ) ), ENT_QUOTES ) ) );
    return $formData;
}

// function that checks if new user is trying to sign up with an email address that already exists in 'user' table
function checkDuplicateEmail( $existingEmail, $signUpEmail ) {
    // include connection
    include('connection.php');
    // creates a query that looks for an entry that matches the email in the $signUpEmail input.
    $query = "SELECT * FROM users WHERE email='$signUpEmail'";
    // storing the result
    $result = mysqli_query( $conn, $query );
    // checks to see if any results are returned
    if ( mysqli_num_rows( $result ) > 0 ) {
        // storing the result for comparison
        while( $row = mysqli_fetch_assoc( $result ) ) {
            $existingEmail  = $row['email'];
            }
    }
    // if the email found in the database matches the email in the input
   if ( $existingEmail == $signUpEmail) {
       // pass the value "error" to $signUpEmail for checking in index.php
       $signUpEmail = "error";
     }
    // if no email was found to be a match then simply return $signUpEmail
    return $signUpEmail;
}

?>